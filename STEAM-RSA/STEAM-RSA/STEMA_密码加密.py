# -*- coding: utf-8 -*-
"""
版权所有：J哥
微信号：wws_0904

https://store.steampowered.com/login/?redir_ssl=1

RSA非对称加密  每次生成的加密 密文是不一样的
"""

import requests
import execjs

url = 'https://store.steampowered.com/login/getrsakey/'
headers = {
    'Cookie': 'browserid=2477518626206238191; timezoneOffset=28800,0; _ga=GA1.2.1282611728.1618717154; steamCountry=CN%7Cb513c61975e737f1455d0222dbd1e1a9; sessionid=440c0c6f658778036eddae86; _gid=GA1.2.1157117822.1630332473',
    'Host': 'store.steampowered.com',
    'Referer': 'https://store.steampowered.com/login/?redir_ssl=1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
}
data = {
    "donotcache": "1630332497306",
    "username": "123@qq.com",
}
response = requests.post(url=url, headers=headers, data=data).json()
publickey_mod = response['publickey_mod']
publickey_exp = response['publickey_exp']
print(publickey_exp, publickey_mod)

js = open('STEAM_RSA.js', encoding='utf-8').read()
ctx = execjs.compile(js)
p = input('请输入密码：')
pwd = ctx.call('getPwd', p, publickey_mod, publickey_exp)
print(pwd)
# node = execjs.get()
# ctx = node.compile(open('STEAM_RSA.js', encoding='utf-8').read())
# funcName = 'getPwd("{0}","{1}","{2}")'.format('123456', publickey_mod, publickey_exp)
# pwd = ctx.eval(funcName)
# print(pwd)
