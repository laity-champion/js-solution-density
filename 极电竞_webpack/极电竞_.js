var get_sign;
var window = global;
// webpack打包形式 根据i()定位到的
!function(e) {
    function t(t) {
        for (var n, o, u = t[0], i = t[1], l = t[2], d = 0, s = []; d < u.length; d++)
            o = u[d],
            Object.prototype.hasOwnProperty.call(a, o) && a[o] && s.push(a[o][0]),
            a[o] = 0;
        for (n in i)
            Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n]);
        for (f && f(t); s.length; )
            s.shift()();
        return c.push.apply(c, l || []),
        r()
    }
    function r() {
        for (var e, t = 0; t < c.length; t++) {
            for (var r = c[t], n = !0, o = 1; o < r.length; o++) {
                var i = r[o];
                0 !== a[i] && (n = !1)
            }
            n && (c.splice(t--, 1),
            e = u(u.s = r[0]))
        }
        return e
    }
    var n = {}
      , o = {
        14: 0
    }
      , a = {
        14: 0
    }
      , c = [];
    function u(t) {
        if (n[t])
            return n[t].exports;
        var r = n[t] = {
            i: t,
            l: !1,
            exports: {}
        };
        return e[t].call(r.exports, r, r.exports, u),
        r.l = !0,
        r.exports
    }
    u.e = function(e) {
        var t = []
          , r = function() {
            try {
                return document.createElement("link").relList.supports("preload")
            } catch (e) {
                return !1
            }
        }();
        o[e] ? t.push(o[e]) : 0 !== o[e] && {
            1: 1,
            2: 1,
            3: 1,
            4: 1,
            5: 1,
            6: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
            16: 1
        }[e] && t.push(o[e] = new Promise((function(t, n) {
            for (var a = "css/" + {
                0: "31d6cfe",
                1: "8adb2fd",
                2: "14a48dc",
                3: "80b3091",
                4: "f5c095b",
                5: "a398206",
                6: "398d084",
                7: "31d6cfe",
                10: "ca619cb",
                11: "5936269",
                12: "cb4ea01",
                13: "f22319f",
                16: "b7e9574"
            }[e] + ".css", c = u.p + a, i = document.getElementsByTagName("link"), l = 0; l < i.length; l++) {
                var d = (s = i[l]).getAttribute("data-href") || s.getAttribute("href");
                if (!("stylesheet" !== s.rel && "preload" !== s.rel || d !== a && d !== c))
                    return t()
            }
            var f = document.getElementsByTagName("style");
            for (l = 0; l < f.length; l++) {
                var s;
                if ((d = (s = f[l]).getAttribute("data-href")) === a || d === c)
                    return t()
            }
            var p = document.createElement("link");
            p.rel = r ? "preload" : "stylesheet",
            r ? p.as = "style" : p.type = "text/css",
            p.onload = t,
            p.onerror = function(t) {
                var r = t && t.target && t.target.src || c
                  , a = new Error("Loading CSS chunk " + e + " failed.\n(" + r + ")");
                a.code = "CSS_CHUNK_LOAD_FAILED",
                a.request = r,
                delete o[e],
                p.parentNode.removeChild(p),
                n(a)
            }
            ,
            p.href = c,
            document.getElementsByTagName("head")[0].appendChild(p)
        }
        )).then((function() {
            if (o[e] = 0,
            r) {
                var t = document.createElement("link");
                t.href = u.p + "css/" + {
                    0: "31d6cfe",
                    1: "8adb2fd",
                    2: "14a48dc",
                    3: "80b3091",
                    4: "f5c095b",
                    5: "a398206",
                    6: "398d084",
                    7: "31d6cfe",
                    10: "ca619cb",
                    11: "5936269",
                    12: "cb4ea01",
                    13: "f22319f",
                    16: "b7e9574"
                }[e] + ".css",
                t.rel = "stylesheet",
                t.type = "text/css",
                document.body.appendChild(t)
            }
        }
        )));
        var n = a[e];
        if (0 !== n)
            if (n)
                t.push(n[2]);
            else {
                var c = new Promise((function(t, r) {
                    n = a[e] = [t, r]
                }
                ));
                t.push(n[2] = c);
                var i, l = document.createElement("script");
                l.charset = "utf-8",
                l.timeout = 120,
                u.nc && l.setAttribute("nonce", u.nc),
                l.src = function(e) {
                    return u.p + "" + {
                        0: "92ce80c",
                        1: "1190d04",
                        2: "ba69f96",
                        3: "232eada",
                        4: "3a3c8d8",
                        5: "eb3b66d",
                        6: "ee967ee",
                        7: "4e8dd11",
                        10: "b280676",
                        11: "eb8f598",
                        12: "8288422",
                        13: "3e27582",
                        16: "47f53ac"
                    }[e] + ".js"
                }(e);
                var d = new Error;
                i = function(t) {
                    l.onerror = l.onload = null,
                    clearTimeout(f);
                    var r = a[e];
                    if (0 !== r) {
                        if (r) {
                            var n = t && ("load" === t.type ? "missing" : t.type)
                              , o = t && t.target && t.target.src;
                            d.message = "Loading chunk " + e + " failed.\n(" + n + ": " + o + ")",
                            d.name = "ChunkLoadError",
                            d.type = n,
                            d.request = o,
                            r[1](d)
                        }
                        a[e] = void 0
                    }
                }
                ;
                var f = setTimeout((function() {
                    i({
                        type: "timeout",
                        target: l
                    })
                }
                ), 12e4);
                l.onerror = l.onload = i,
                document.head.appendChild(l)
            }
        return Promise.all(t)
    }
    ,
    u.m = e,
    u.c = n,
    u.d = function(e, t, r) {
        u.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: r
        })
    }
    ,
    u.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }
    ,
    u.t = function(e, t) {
        if (1 & t && (e = u(e)),
        8 & t)
            return e;
        if (4 & t && "object" == typeof e && e && e.__esModule)
            return e;
        var r = Object.create(null);
        if (u.r(r),
        Object.defineProperty(r, "default", {
            enumerable: !0,
            value: e
        }),
        2 & t && "string" != typeof e)
            for (var n in e)
                u.d(r, n, function(t) {
                    return e[t]
                }
                .bind(null, n));
        return r
    }
    ,
    u.n = function(e) {
        var t = e && e.__esModule ? function() {
            return e.default
        }
        : function() {
            return e
        }
        ;
        return u.d(t, "a", t),
        t
    }
    ,
    u.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }
    ,
    u.p = "https://cdn.jdj007.com/_nuxt/",
    u.oe = function(e) {
        throw e
    }
    ;
    var i = window.webpackJsonp = window.webpackJsonp || []
      , l = i.push.bind(i);
    i.push = t,
    i = i.slice();
    for (var d = 0; d < i.length; d++)
        t(i[d]);
    var f = l;
    r()
    get_sign = u;
}(
{175: function(e, t, n) {
    var r;
    e.exports = (r = r || function(e, t) {
        var n = Object.create || function() {
            function e() {}
            return function(t) {
                var n;
                return e.prototype = t,
                n = new e,
                e.prototype = null,
                n
            }
        }()
          , r = {}
          , i = r.lib = {}
          , a = i.Base = {
            extend: function(e) {
                var t = n(this);
                return e && t.mixIn(e),
                t.hasOwnProperty("init") && this.init !== t.init || (t.init = function() {
                    t.$super.init.apply(this, arguments)
                }
                ),
                t.init.prototype = t,
                t.$super = this,
                t
            },
            create: function() {
                var e = this.extend();
                return e.init.apply(e, arguments),
                e
            },
            init: function() {},
            mixIn: function(e) {
                for (var t in e)
                    e.hasOwnProperty(t) && (this[t] = e[t]);
                e.hasOwnProperty("toString") && (this.toString = e.toString)
            },
            clone: function() {
                return this.init.prototype.extend(this)
            }
        }
          , o = i.WordArray = a.extend({
            init: function(e, n) {
                e = this.words = e || [],
                this.sigBytes = n != t ? n : 4 * e.length
            },
            toString: function(e) {
                return (e || l).stringify(this)
            },
            concat: function(e) {
                var t = this.words
                  , n = e.words
                  , r = this.sigBytes
                  , i = e.sigBytes;
                if (this.clamp(),
                r % 4)
                    for (var a = 0; a < i; a++) {
                        var o = n[a >>> 2] >>> 24 - a % 4 * 8 & 255;
                        t[r + a >>> 2] |= o << 24 - (r + a) % 4 * 8
                    }
                else
                    for (a = 0; a < i; a += 4)
                        t[r + a >>> 2] = n[a >>> 2];
                return this.sigBytes += i,
                this
            },
            clamp: function() {
                var t = this.words
                  , n = this.sigBytes;
                t[n >>> 2] &= 4294967295 << 32 - n % 4 * 8,
                t.length = e.ceil(n / 4)
            },
            clone: function() {
                var e = a.clone.call(this);
                return e.words = this.words.slice(0),
                e
            },
            random: function(t) {
                for (var n, r = [], i = function(t) {
                    t = t;
                    var n = 987654321
                      , r = 4294967295;
                    return function() {
                        var i = ((n = 36969 * (65535 & n) + (n >> 16) & r) << 16) + (t = 18e3 * (65535 & t) + (t >> 16) & r) & r;
                        return i /= 4294967296,
                        (i += .5) * (e.random() > .5 ? 1 : -1)
                    }
                }, a = 0; a < t; a += 4) {
                    var s = i(4294967296 * (n || e.random()));
                    n = 987654071 * s(),
                    r.push(4294967296 * s() | 0)
                }
                return new o.init(r,t)
            }
        })
          , s = r.enc = {}
          , l = s.Hex = {
            stringify: function(e) {
                for (var t = e.words, n = e.sigBytes, r = [], i = 0; i < n; i++) {
                    var a = t[i >>> 2] >>> 24 - i % 4 * 8 & 255;
                    r.push((a >>> 4).toString(16)),
                    r.push((15 & a).toString(16))
                }
                return r.join("")
            },
            parse: function(e) {
                for (var t = e.length, n = [], r = 0; r < t; r += 2)
                    n[r >>> 3] |= parseInt(e.substr(r, 2), 16) << 24 - r % 8 * 4;
                return new o.init(n,t / 2)
            }
        }
          , d = s.Latin1 = {
            stringify: function(e) {
                for (var t = e.words, n = e.sigBytes, r = [], i = 0; i < n; i++) {
                    var a = t[i >>> 2] >>> 24 - i % 4 * 8 & 255;
                    r.push(String.fromCharCode(a))
                }
                return r.join("")
            },
            parse: function(e) {
                for (var t = e.length, n = [], r = 0; r < t; r++)
                    n[r >>> 2] |= (255 & e.charCodeAt(r)) << 24 - r % 4 * 8;
                return new o.init(n,t)
            }
        }
          , c = s.Utf8 = {
            stringify: function(e) {
                try {
                    return decodeURIComponent(escape(d.stringify(e)))
                } catch (e) {
                    throw new Error("Malformed UTF-8 data")
                }
            },
            parse: function(e) {
                return d.parse(unescape(encodeURIComponent(e)))
            }
        }
          , u = i.BufferedBlockAlgorithm = a.extend({
            reset: function() {
                this._data = new o.init,
                this._nDataBytes = 0
            },
            _append: function(e) {
                "string" == typeof e && (e = c.parse(e)),
                this._data.concat(e),
                this._nDataBytes += e.sigBytes
            },
            _process: function(t) {
                var n = this._data
                  , r = n.words
                  , i = n.sigBytes
                  , a = this.blockSize
                  , s = i / (4 * a)
                  , l = (s = t ? e.ceil(s) : e.max((0 | s) - this._minBufferSize, 0)) * a
                  , d = e.min(4 * l, i);
                if (l) {
                    for (var c = 0; c < l; c += a)
                        this._doProcessBlock(r, c);
                    var u = r.splice(0, l);
                    n.sigBytes -= d
                }
                return new o.init(u,d)
            },
            clone: function() {
                var e = a.clone.call(this);
                return e._data = this._data.clone(),
                e
            },
            _minBufferSize: 0
        })
          , p = (i.Hasher = u.extend({
            cfg: a.extend(),
            init: function(e) {
                this.cfg = this.cfg.extend(e),
                this.reset()
            },
            reset: function() {
                u.reset.call(this),
                this._doReset()
            },
            update: function(e) {
                return this._append(e),
                this._process(),
                this
            },
            finalize: function(e) {
                return e && this._append(e),
                this._doFinalize()
            },
            blockSize: 16,
            _createHelper: function(e) {
                return function(t, n) {
                    return new e.init(n).finalize(t)
                }
            },
            _createHmacHelper: function(e) {
                return function(t, n) {
                    return new p.HMAC.init(e,n).finalize(t)
                }
            }
        }),
        r.algo = {});
        return r
    }(Math),
    r)
}//175
    ,202: function(e, t, n) {
    var r;
    e.exports = (r = n(175),
    function(e) {
        var t = r
          , n = t.lib
          , i = n.WordArray
          , a = n.Hasher
          , o = t.algo
          , s = []
          , l = [];
        !function() {
            function t(t) {
                for (var n = e.sqrt(t), r = 2; r <= n; r++)
                    if (!(t % r))
                        return !1;
                return !0
            }
            function n(e) {
                return 4294967296 * (e - (0 | e)) | 0
            }
            for (var r = 2, i = 0; i < 64; )
                t(r) && (i < 8 && (s[i] = n(e.pow(r, .5))),
                l[i] = n(e.pow(r, 1 / 3)),
                i++),
                r++
        }();
        var d = []
          , c = o.SHA256 = a.extend({
            _doReset: function() {
                this._hash = new i.init(s.slice(0))
            },
            _doProcessBlock: function(e, t) {
                for (var n = this._hash.words, r = n[0], i = n[1], a = n[2], o = n[3], s = n[4], c = n[5], u = n[6], p = n[7], f = 0; f < 64; f++) {
                    if (f < 16)
                        d[f] = 0 | e[t + f];
                    else {
                        var h = d[f - 15]
                          , v = (h << 25 | h >>> 7) ^ (h << 14 | h >>> 18) ^ h >>> 3
                          , m = d[f - 2]
                          , g = (m << 15 | m >>> 17) ^ (m << 13 | m >>> 19) ^ m >>> 10;
                        d[f] = v + d[f - 7] + g + d[f - 16]
                    }
                    var b = r & i ^ r & a ^ i & a
                      , y = (r << 30 | r >>> 2) ^ (r << 19 | r >>> 13) ^ (r << 10 | r >>> 22)
                      , w = p + ((s << 26 | s >>> 6) ^ (s << 21 | s >>> 11) ^ (s << 7 | s >>> 25)) + (s & c ^ ~s & u) + l[f] + d[f];
                    p = u,
                    u = c,
                    c = s,
                    s = o + w | 0,
                    o = a,
                    a = i,
                    i = r,
                    r = w + (y + b) | 0
                }
                n[0] = n[0] + r | 0,
                n[1] = n[1] + i | 0,
                n[2] = n[2] + a | 0,
                n[3] = n[3] + o | 0,
                n[4] = n[4] + s | 0,
                n[5] = n[5] + c | 0,
                n[6] = n[6] + u | 0,
                n[7] = n[7] + p | 0
            },
            _doFinalize: function() {
                var t = this._data
                  , n = t.words
                  , r = 8 * this._nDataBytes
                  , i = 8 * t.sigBytes;
                return n[i >>> 5] |= 128 << 24 - i % 32,
                n[14 + (i + 64 >>> 9 << 4)] = e.floor(r / 4294967296),
                n[15 + (i + 64 >>> 9 << 4)] = r,
                t.sigBytes = 4 * n.length,
                this._process(),
                this._hash
            },
            clone: function() {
                var e = a.clone.call(this);
                return e._hash = this._hash.clone(),
                e
            }
        });
        t.SHA256 = a._createHelper(c),
        t.HmacSHA256 = a._createHmacHelper(c)
    }(Math),
    r.SHA256)
}//202

, 203: function(e, t, n) {
    var r;
    e.exports = (r = n(175),
    function() {
        var e = r
          , t = e.lib.WordArray;
        function n(e, n, r) {
            for (var i = [], a = 0, o = 0; o < n; o++)
                if (o % 4) {
                    var s = r[e.charCodeAt(o - 1)] << o % 4 * 2
                      , l = r[e.charCodeAt(o)] >>> 6 - o % 4 * 2;
                    i[a >>> 2] |= (s | l) << 24 - a % 4 * 8,
                    a++
                }
            return t.create(i, a)
        }
        e.enc.Base64 = {
            stringify: function(e) {
                var t = e.words
                  , n = e.sigBytes
                  , r = this._map;
                e.clamp();
                for (var i = [], a = 0; a < n; a += 3)
                    for (var o = (t[a >>> 2] >>> 24 - a % 4 * 8 & 255) << 16 | (t[a + 1 >>> 2] >>> 24 - (a + 1) % 4 * 8 & 255) << 8 | t[a + 2 >>> 2] >>> 24 - (a + 2) % 4 * 8 & 255, s = 0; s < 4 && a + .75 * s < n; s++)
                        i.push(r.charAt(o >>> 6 * (3 - s) & 63));
                var l = r.charAt(64);
                if (l)
                    for (; i.length % 4; )
                        i.push(l);
                return i.join("")
            },
            parse: function(e) {
                var t = e.length
                  , r = this._map
                  , i = this._reverseMap;
                if (!i) {
                    i = this._reverseMap = [];
                    for (var a = 0; a < r.length; a++)
                        i[r.charCodeAt(a)] = a
                }
                var o = r.charAt(64);
                if (o) {
                    var s = e.indexOf(o);
                    -1 !== s && (t = s)
                }
                return n(e, t, i)
            },
            _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
        }
    }(),
    r.enc.Base64)//203
}//
});  // 改

function getSign(p) {
    var wws = p;
    var r = (new Date).getTime(),
        o = "timestamp=".concat(r, "&secret=").concat("aHVheWluZ19zZWNyZXRfYXBp")
        a = get_sign(202)
        e = o   //timestamp=1631344889846&secret=aHVheWluZ19zZWNyZXRfYXBp  时间戳拼接没有问题
        // console.log(e)
        i = get_sign.n(a)
        c = get_sign(203)
        s = get_sign.n(c)
        n = i()(e)
        a = encodeURIComponent(s.a.stringify(n))
        var sign =  a
    return sign;
}

console.log(getSign('q'))


