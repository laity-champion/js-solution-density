# -*- coding: utf-8 -*-
"""
版权所有：J哥
微信号：wws_0904

https://creator.jr.jd.com/#/login

分析:
RSA加密
"""
import binascii
import execjs
pwd = '\u8d26\u6237\u540d\u4e0e\u5bc6\u7801\u4e0d\u5339\u914d\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165'
ctx = execjs.compile(open('京东金融_RSA.js', encoding='utf-8').read())
password = input('请输入密码:')
p = ctx.call('getPwd', password)
print(p)

