# -*- coding: utf-8 -*-
"""
版权所有：J哥
微信号：wws_0904
"""
from pprint import pprint
import execjs

import requests

# 读取js代码
i = input('输入你想搜索的中文：').strip()

with open('有道Fy.js', encoding='utf-8') as f:
    js_code = f.read()
# 编译js代码，调用代码里面的方法
ctx = execjs.compile(js_code)
encode_data = ctx.call('trans', i)
print(encode_data)

url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'

headers = {
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    # 'Content-Length': '264',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Cookie': '_ntes_nnid=db7d19bf55f2ff1e4b76deac83106071,1620299791917; OUTFOX_SEARCH_USER_ID_NCOO=116323559.65248479; OUTFOX_SEARCH_USER_ID="1989851808@10.108.160.19"; JSESSIONID=aaapc0z00sWVA-ixtylSx; fanyi-ad-id=113723; fanyi-ad-closed=1; ___rl__test__cookies=1627988090226',
    'Host': 'fanyi.youdao.com',
    'Origin': 'https://fanyi.youdao.com',
    'Pragma': 'no-cache',
    'Referer': 'https://fanyi.youdao.com/',
    'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    'sec-ch-ua-mobile': '?0',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest',
}
data = {
    'i': i,
    'from': 'AUTO',
    'to': 'AUTO',
    'smartresult': 'dict',
    'client': 'fanyideskweb',
    # 'salt': '16279836081742',
    # 'sign': '782f4a29a48acddfea0afd601108aed9',
    # 'lts': '1627983608174',
    # 'bv': 'e8f74db749b4a06c7bd041e0d09507d4',
    'doctype': 'json',
    'version': '2.1',
    'keyfrom': 'fanyi.web',
    'action': 'FY_BY_REALTlME',
}
data.update(encode_data)
resp = requests.post(url=url, headers=headers, data=data).json()['translateResult'][0][0]['tgt']
pprint(resp)
